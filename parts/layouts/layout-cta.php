<?php
/**
 * Description: Lionlab cta
 *
 * @package Lionlab
 * @subpackage Lionlab
 * @since Version 1.0
 * @author Kaspar Rudbech
*/

//sections settings
$img = get_sub_field('bg_img');
$margin = get_sub_field('margin');
$overlay = get_sub_field('bg_overlay');
$anchor_id = get_sub_field('id');

$header = get_sub_field('header');
$text = get_sub_field('text');
?>

<section id="<?php echo esc_attr($anchor_id); ?>" class="cta margin--<?php echo esc_attr($margin); ?>">
  <?php if ($overlay === true) : ?>
  <div class="wrap hpad cta__container cta__container--overlay" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
  <?php else : ?>
  <div class="wrap hpad cta__container" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
  <?php endif; ?>
      <div class="row cta__row">

        <div class="col-sm-8 col-sm-offset-2 center cta__col"> 
            <h2 class="cta__title"><?php echo esc_html($header); ?></h2>
            <?php echo $text; ?>
        </div>

      </div>
  </div>    
</section>
