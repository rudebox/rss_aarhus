<?php
 /**
   * Description: Lionlab employees repeater field group
   *
   * @package Lionlab
   * @subpackage Lionlab
   * @since Version 1.0
   * @author Kaspar Rudbech
   */
 

 //section settings
$header = get_sub_field('header');
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');

 if (have_rows('employees') ) :
?>

<section class="employees  bg--<?php echo esc_attr($bg); ?> padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<?php if ($header) : ?>
			<h2 class="employees__header"><?php echo esc_html($header); ?></h2>
		<?php endif; ?>
		<?php if ($text) : ?>
			<div class="employees__header-text center">
				<?php echo $text; ?>
			</div>
		<?php endif; ?>
		<div class="row clearfix flex flex--wrap">
		<?php while (have_rows('employees') ) : the_row(); 
			$img = get_sub_field('img');
			$name = get_sub_field('name');
			$position = get_sub_field('position');
			$mail = get_sub_field('mail');
			$phone = get_sub_field('phone');
		?>

		<div class="col-sm-4 employees__item">
			<?php if ($img) : ?>
			<div class="employees__img" style="background-image: url(<?php echo esc_url($img['url']); ?>);"></div>
			<?php endif; ?>
			<div class="employees__content">
				<?php if ($name) : ?>
				<h5 class="employees__name"><?php echo esc_html($name); ?></h5>
				<?php endif; ?>
				<?php if ($position) : ?>
				<h6 class="employees__position red"><?php echo esc_html($position); ?></h6>
				<?php endif; ?>
				
				<?php if ($mail) : ?>
				 E-mail: <a class="employees__mail" href="mailto:<?php echo esc_html($mail); ?>"><?php echo esc_html($mail); ?></a><br>
				<?php endif; ?>

				<?php if ($phone) : ?>
				Tlf: <a class="employees__phone" href="tel:<?php echo get_formatted_phone($phone); ?>"><?php echo esc_html($phone); ?></a>
				<?php endif; ?>
				
			</div>
		</div>
		<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>