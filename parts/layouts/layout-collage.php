<?php 

//section settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');

//fields
$text = get_sub_field('collage_text');
$title = get_sub_field('collage_header');
?>

<section class="collage bg--<?php echo esc_attr($bg); ?> padding--<?php echo esc_attr($margin); ?>">
  <div class="collage__container">
    <div class="collage__row">

      <div class="collage__text">
        <h2 class="collage__title"><?php echo $title; ?></h2>
        <?php echo $text; ?>
      </div>

      <div class="collage__element">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/collage.png" alt="rudolf_steiner_skole_collage">
        <img class="collage__element--circle" src="<?php echo get_template_directory_uri(); ?>/assets/img/circle.png" alt="rudolf_steiner_skole_collage_circle">
      </div>

    </div>
  </div>
</section>
