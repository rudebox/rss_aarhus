<?php 
/**
* Description: Lionlab ig-feed field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$margin = get_sub_field('margin');
$title = get_sub_field('feed_header');

$link = get_sub_field('feed_link');
$link_text = get_sub_field('feed_link_text');
$script = get_sub_field('feed_script');
?>

<section class="feed padding--<?php echo esc_attr($margin); ?>">
	<div class="feed__container">

		<?php if ($title) : ?>
		<h2 class="feed__title"><?php echo esc_html($title); ?></h2>
		<?php endif; ?>

		<?php echo $script; ?>

		<?php if ($link) : ?>
		<div class="feed__btn">
			<a target="_blank" rel="noopener" class="btn btn--red" href="<?php echo esc_url($link); ?>"><?php echo esc_html($link_text); ?></a>
		</div>
		<?php endif; ?>
	</div>
	<div class="feed__bg">
		<?php echo file_get_contents(get_template_directory_uri() . '/assets/img/bg.svg'); ?>
	</div>
</section>