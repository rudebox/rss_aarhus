<?php
/**
 * Description: Lionlab sliders
 *
 * @package Lionlab
 * @subpackage Lionlab
 * @since Version 1.0
 * @author Kaspar Rudbech
*/

if ( have_rows('slides') ) : ?>

  <section class="slider">
    <div class="slider__track is-slider">

      <?php
      // Loop through slides
      while ( have_rows('slides') ) :
        the_row();
        $type = get_sub_field('slides_type');
        $image   = get_sub_field('slides_bg');
        $video   = get_sub_field('slides_video');
        $video_thumbnail  = get_sub_field('slides_video_thumnail');
        $title = get_sub_field('slides_title');
        $caption = get_sub_field('slides_text'); 
        $overlay = get_sub_field('slides_overlay'); 
        ?>

        <?php if ($type === 'img') : ?>

        <?php if ($overlay === true) : ?>
        <div class="slider__item slider__item--overlay flex flex--valign" style="background-image: url(<?php echo esc_url($image['url']); ?>);">
        <?php else : ?>
        <div class="slider__item flex flex--valign" style="background-image: url(<?php echo esc_url($image['url']); ?>);">
        <?php endif; ?>
          <div class="wrap hpad slider__container">
            <div class="slider__text">
              <h2 class="slider__title"><?php echo esc_html($title); ?></h2>
              <?php echo $caption; ?>
            </div>
          </div>
        </div>

        <?php else : ?>

        <?php if ($overlay === true) : ?>
        <div class="slider__item slider__item--overlay flex flex--valign" style="background-image: url(<?php echo esc_url($video_thumbnail['url']); ?>);">
        <?php else : ?>
        <div class="slider__item flex flex--valign" style="background-image: url(<?php echo esc_url($video_thumbnail['url']); ?>);">
        <?php endif; ?>
          <div id="ytbg" data-youtube="<?php echo esc_url($video); ?>"></div>
          <div class="wrap hpad slider__container">
            <div class="slider__text">
              <h2 class="slider__title"><?php echo esc_html($title); ?></h2>
              <?php echo $caption; ?>
            </div>
          </div>
        </div>
      <?php endif; ?>

      <?php endwhile; ?>

    </div>
    <?php echo file_get_contents(get_template_directory_uri() . '/assets/img/wave.svg'); ?>
  </section>
<?php endif; ?>