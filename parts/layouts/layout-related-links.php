<?php 
  //sections settings
  $margin = get_sub_field('margin');
  $bg = get_sub_field('bg');
  $header = get_sub_field('header');
  $bg_illlustration = get_sub_field('bg_illustration');

  if (have_rows('related_links') ) :
?>

<section class="related-links bg--<?php echo esc_attr($bg); ?> padding--<?php echo esc_attr($margin); ?>">

    <div class="related-links__container">
      <?php if ($header) : ?>
      <h2><?php echo esc_html($header); ?></h2>
      <?php endif; ?>
      <div class="related-links__row">
          <?php while (have_rows('related_links') ) : the_row(); 
            $title = get_sub_field('title');
            $link = get_sub_field('link');
            $link_external = get_sub_field('link_external');
            $link_anchor = get_sub_field('link_anchor');
            $icon = get_sub_field('icon');

            $link_type = get_sub_field('link_type');
          ?>

          <?php if ($link_type === 'intern') : ?>
          <a class="related-links__item anim fade-up" style="background-image: url(<?php echo esc_url($icon['url']); ?>);" href="<?php echo esc_url($link);  ?>">
          <?php elseif ($link_type === 'external') : ?>
          <a class="related-links__item anim fade-up" style="background-image: url(<?php echo esc_url($icon['url']); ?>);" target="_blank" rel="noopener" href="<?php echo esc_url($link_external);  ?>">
          <?php else : ?>
          <a class="related-links__item anim fade-up" style="background-image: url(<?php echo esc_url($icon['url']); ?>);" href="<?php echo esc_url($link_anchor);  ?>">
          <?php endif; ?>
            <div class="related-links__wrap">
              <h6 class="related-links__title"><?php echo esc_html($title); ?></h6>
            </div>
          </a>
        <?php endwhile; ?>
      </div>
      <?php if ($bg_illlustration === true) : ?>
      <div class="related-links__bg">
        <?php echo file_get_contents(get_template_directory_uri() . '/assets/img/bg.svg'); ?>
      </div>
      <?php endif; ?>
    </div>

</section>
<?php endif; ?>
