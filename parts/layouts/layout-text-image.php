<?php
/**
* Description: Lionlab text-image field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//text column
$title = get_sub_field('header');
$position = get_sub_field('position');
$text = get_sub_field('text');

//section settings
$margin = get_sub_field('margin');
$bg = get_sub_field('bg');
$bg_illlustration = get_sub_field('bg_illustration');
$img = get_sub_field('img');
$anchor_id = get_sub_field('id');

if ($position === 'left') {
	$row_class = 'flex--reverse';
}

?>


<div class="text-image__wrapper" id="<?php echo esc_attr($anchor_id); ?>">
	<section class="text-image padding--<?php echo esc_attr($margin); ?> bg--<?php echo esc_attr($bg); ?>"> 
		<div class="wrap hpad">
			<div class="row flex flex--wrap <?php echo esc_attr($row_class); ?>">

				<div class="text-image__content col-sm-6 <?php echo esc_attr($position); ?>">
					
					<?php if ($title) : ?>
						<h2 class="text-image__title"><?php echo $title; ?></h2>
					<?php endif; ?>

					<?php echo $text; ?>
				</div>
				
				<?php if ($img) : ?>
				<div loading="lazy" class="text-image__image anim fade-up col-sm-6 <?php echo esc_attr($position); ?>" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
			    </div>
				<?php endif; ?>

			</div>			
		</div>
		<?php if ($bg_illlustration === true && $position === 'right') : ?>
		<div class="text-image__bg">
			<?php echo file_get_contents(get_template_directory_uri() . '/assets/img/bg.svg'); ?>
		</div>
		<?php endif; ?>
		<?php if ($bg_illlustration === true && $position === 'left') : ?>
		<div class="text-image__bg text-image__bg--left">
			<?php echo file_get_contents(get_template_directory_uri() . '/assets/img/bg.svg'); ?>		
		</div>
		<?php endif; ?>
	</section>
</div>
