<?php 
/**
* Description: Lionlab gallery field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//gallery
$gallery = get_sub_field('gallery'); 
$index = get_row_index();


//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$center = get_sub_field('header_center');

if ($center === true) {
	$class = 'center';
}


if ( $gallery ) : ?> 

	<section class="gallery padding--<?php echo esc_attr($margin); ?> bg--<?php echo esc_attr($bg); ?>" itemscope itemtype="ImageGallery">
		<div class="wrap hpad">
			
			<?php if ($title) : ?>
			<h2 class="gallery__title <?php echo esc_attr($class); ?>"><?php echo esc_html($title); ?></h2>
			<?php endif; ?>

			<div class="gallery__list flex flex--wrap">

				<?php
					// Loop through gallery
					foreach ( $gallery as $image ) : 
				?>


					<figure class="gallery__item" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" style="background-image: url(<?php echo esc_url($image['sizes']['gallery']); ?>);">
						<a href="<?= $image['sizes']['large']; ?>" class="gallery__link no-ajax" data-fancybox="gallery-<?= $index; ?>" itemprop="contentUrl">
						</a>
					</figure>
				<?php endforeach; ?>

			</div>
		</div>
		<div class="feed__bg">
		<?php echo file_get_contents(get_template_directory_uri() . '/assets/img/bg.svg'); ?>
		</div>
	</section>

<?php endif; ?>