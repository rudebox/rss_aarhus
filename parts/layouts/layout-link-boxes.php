<?php 
/**
* Description: Lionlab link-boxes repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

if (have_rows('linkbox') ) :
?>

<section class="link-boxes <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>">
	<div class="link-boxes__container">
		<h2 class="link-boxes__header"><?php echo esc_html($title); ?></h2>
		<div class="link-boxes__row">
			<?php while (have_rows('linkbox') ) : the_row(); 
				$title = get_sub_field('title');
				$link = get_sub_field('link');
				$icon = get_sub_field('icon');
				$link_external = get_sub_field('link_external');
            	$link_anchor = get_sub_field('link_anchor');

            $link_type = get_sub_field('link_type');
			?>

			<?php if ($link_type === 'intern') : ?>
			<a href="<?php echo esc_url($link); ?>" class="col-sm-4 link-boxes__item anim fade-up" style="background-image: url(<?php echo esc_url($icon['url']); ?>);">
			<?php elseif ($link_type === 'external') : ?>
			<a href="<?php echo esc_url($link_external); ?>" target="_blank" rel="noopener" class="col-sm-4 link-boxes__item anim fade-up" style="background-image: url(<?php echo esc_url($icon['url']); ?>);">
			<?php else : ?>
			<a href="<?php echo esc_url($link_anchor); ?>" class="col-sm-4 link-boxes__item anim fade-up" style="background-image: url(<?php echo esc_url($icon['url']); ?>);">
			<?php endif; ?>
				<div class="link-boxes__wrap">
					<h2 class="link-boxes__title h3"><?php echo esc_html($title); ?></h2>
				</div>
			</a>
			<?php endwhile; ?>
		</div>
		<?php if (is_front_page() ) : ?>
		<div class="link-boxes__bg">
			<?php echo file_get_contents(get_template_directory_uri() . '/assets/img/bg.svg'); ?>
		</div>
		<?php endif; ?>
	</div>
	<?php if (is_front_page() ) : ?>
	<div class="link-boxes__pointer anim">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/maler_haand_red.png" alt="rudolf_steiner_haand">
	</div>
	<div class="link-boxes__branch">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/beech_branch.png" alt="">
	</div>
	<?php endif; ?>
</section>
<?php endif; ?>