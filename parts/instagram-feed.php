<section class="feed">
	<div class="feed__container">
		<?php 
			$title = get_field('ig_title', 'options');
			$ig_link = get_field('ig', 'options');
			$ig_text = get_field('ig_link_text', 'options');
		 ?>

		<?php if ($title) : ?>
		<h2 class="feed__title"><?php echo esc_html($title); ?></h2>
		<?php endif; ?>

		<iframe src="https://cdn.lightwidget.com/widgets/7c9582c514295da2bd1f2229bbf37d87.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width:100%;border:0;overflow:hidden;"></iframe>

		<?php if ($ig_text) : ?>
		<div class="feed__btn">
			<a target="_blank" rel="noopener" class="btn btn--red" href="<?php echo esc_url($ig_link); ?>"><?php echo esc_html($ig_text); ?></a>
		</div>
		<?php endif; ?>
	</div>
	<div class="feed__bg">
		<?php echo file_get_contents(get_template_directory_uri() . '/assets/img/bg.svg'); ?>
	</div>
</section>