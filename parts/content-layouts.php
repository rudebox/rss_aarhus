<?php
if( have_rows('layout') ) {
  $GLOBALS['layout_count'] = 1; while ( have_rows('layout') ) { the_row();
    if( get_row_layout() === 'slider' ) { ?>

      <?php get_template_part( 'parts/layouts/layout', 'slider' ); ?>

    <?php
    } elseif( get_row_layout() === 'link-boxes' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'link-boxes' ); ?>

    <?php
    } elseif( get_row_layout() === 'collage' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'collage' ); ?>

    <?php
    } elseif( get_row_layout() === 'text-image' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'text-image' ); ?>

    <?php
    } elseif( get_row_layout() === 'related-links' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'related-links' ); ?>

    <?php
    } elseif( get_row_layout() === 'employees' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'employees' ); ?>

    <?php
    } elseif( get_row_layout() === 'cta' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'cta' ); ?>

    <?php
    } elseif( get_row_layout() === 'gallery' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'gallery' ); ?>

    <?php
    } elseif( get_row_layout() === 'ig-feed' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'ig-feed' ); ?>

    <?php
    } elseif( get_row_layout() === 'wysiwygs' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'wysiwygs' ); ?>

    <?php
    }
    ?>

  <?php
    $GLOBALS['layout_count']++;
  }
  ?>

<?php
} else {
?>
  <p class="center">You haven't added any layouts yet. <?php edit_post_link('Add one now.'); ?></p>
<?php
}
?>
