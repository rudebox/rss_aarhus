<footer class="footer" itemscope itemtype="http://schema.org/WPFooter">
	<?php echo file_get_contents(get_template_directory_uri() . '/assets/img/wave.svg'); ?>
	<div class="footer__container">
		<div class="footer__row">
			<?php 
				if (have_rows('footer_columns', 'options') ) : while (have_rows('footer_columns', 'options') ) :
					the_row();
				$title = get_sub_field('column_title');
				$text = get_sub_field('column_text');
			 ?>

			 <div class="footer__item">
			 	<?php if ($title) : ?>
			 	<h4 class="footer__title"><?php echo esc_html($title); ?></h4>
			 	<?php endif; ?>
				<?php echo $text; ?>
			 </div>

			<?php endwhile; endif; ?>
		</div>
	</div>
</footer>
<script src="https://cdn.lightwidget.com/widgets/lightwidget.js"></script>
<?php wp_footer(); ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		//YT video bg
	    $(function(){
	        $('[data-youtube]').youtube_background(); 
	    });

	    $('iframe[src*="youtube"]').wrap("<div class='embed embed--16-9'><div class='embed__item'></div>");
    });
</script>
</div>
</div>

</body>
</html>
