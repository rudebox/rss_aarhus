<?php 
/*
 * Template Name: Kontakt
 */

get_template_part('parts/header'); the_post(); 


$text = get_field('contact_text');
$title = get_field('contact_title');
$form_id = get_field('contact_form_id');
?>


<main>
	<?php get_template_part('parts/page', 'header'); ?>

	<section class="contact padding--both">
		<div class="wrap hpad">
			<div class="row">

				<div class="col-sm-6 contact__text">
					<h2><?php echo $title; ?></h2>
					<?php echo $text; ?>
				</div>

				<div class="col-sm-6 contact__form">
					<?php gravity_form( $form_id, $display_title = false, $display_description = true, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); 
					?>
				</div>

			</div>
		</div>
	</section>		

	<?php get_template_part('parts/contact', 'persons'); ?>
</main>

<?php get_template_part('parts/footer'); ?>