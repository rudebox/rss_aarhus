<?php 
	if ( is_tax() ) {
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$title = $term->name;
	} elseif ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_archive() ) {
		$title = post_type_archive_title( '', false );
	} elseif ( is_404() ) {
		$title = __('Siden kunne ikke findes', 'lionlab');
	} elseif ( is_search() ) {
		$title = __('Søgeresultat', 'lionlab');
	} else {
		$id = (is_home()) ? get_option('page_for_posts') : $post->ID;
		$title = get_proper_title($id);
	}

	$img = get_field('page_img') ? : $img = get_field('page_img', 'options') ;

	$add_slides = get_field('add_slider');

	$overlay = get_field('page_img_overlay'); 
	
?>

<?php if ($add_slides === false || is_home() || is_single() || is_archive() || is_search() ) : ?>
<?php if ($overlay === true || is_home() || is_single() || is_archive() || is_search() ) : ?>
<section class="page__hero page__hero--overlay" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
<?php else : ?>
<section class="page__hero" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
<?php endif; ?>
	<div class="page__container">
		<h1 class="page__title"><?php echo $title; ?></h1>
	</div>
	<?php echo file_get_contents(get_template_directory_uri() . '/assets/img/wave.svg'); ?>
</section>
<?php endif; ?>

<?php if ( have_rows('page_slider') && $add_slides === true ) : ?>

  <section class="slider slider--hero">
    <div class="slider__track is-slider">

      <?php
      // Loop through slides
      while ( have_rows('page_slider') ) :
        the_row();
        $type = get_sub_field('page_slider_type');
        $image = get_sub_field('page_slider_img');
        $text = get_sub_field('page_slider_text');
        $video = get_sub_field('page_slider_video_link');
        $video_thumbnail = get_sub_field('page_slider_video_thumbnail');
        $slides_overlay = get_sub_field('page_slider_overlay'); 
      ?>

      <?php if ($type === 'img') : ?>

      	<?php if ($slides_overlay === true) : ?>
        <div class="slider__item slider__item--overlay flex flex--valign" style="background-image: url(<?php echo esc_url($image['url']); ?>);">
        <?php else : ?>
        <div class="slider__item flex flex--valign" style="background-image: url(<?php echo esc_url($image['url']); ?>);">
        <?php endif; ?>
        	<?php if ($text) : ?>
        	<div class="slider__text slider__text--hero"><?php echo $text; ?></div>
        	<?php endif; ?>
        </div>

      <?php else : ?>

      	<?php if ($slides_overlay === true) : ?>
	  	<div class="slider__item slider__item--overlay flex flex--valign" style="background-image: url(<?php echo esc_url($video_thumbnail['url']); ?>);">
	  	<?php else : ?>
		<div class="slider__item flex flex--valign" style="background-image: url(<?php echo esc_url($video_thumbnail['url']); ?>);">
	  	<?php endif; ?>
	      <div id="ytbg" data-youtube="<?php echo esc_url($video); ?>"></div>

	    </div>

	  <?php endif; ?>

      <?php endwhile; ?>

    </div>

	<div class="wrap hpad slider__container">
		<div class="slider__text">
			<h1 class="page__title slider__title"><?php echo $title; ?></h1>
		</div>
	</div>

    <?php echo file_get_contents(get_template_directory_uri() . '/assets/img/wave.svg'); ?>
  </section>
<?php endif; ?>