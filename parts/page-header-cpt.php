<?php 
	if ( is_tax() ) {
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$title = $term->name;
	} elseif ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_archive() ) {
		$title = post_type_archive_title( '', false );
	} elseif ( is_404() ) {
		$title = __('Siden kunne ikke findes', 'lionlab');
	} elseif ( is_search() ) {
		$title = __('Søgeresultat', 'lionlab');
	} else {
		$id = (is_home()) ? get_option('page_for_posts') : $post->ID;
		$title = get_proper_title($id);
	}

	$page_img_employee = get_field('page_img_employee', 'options');

	$img = get_field('page_img', 'options') ;
	
?>
<?php if (is_post_type_archive('medarbejder') && !empty ($page_img_employee) ) : ?>
<section class="page__hero page__hero--overlay" style="background-image: url(<?php echo esc_url($page_img_employee['url']); ?>);">
<?php else : ?>
<section class="page__hero page__hero--overlay" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
<?php endif; ?>
	<div class="page__container">
		<h1 class="page__title"><?php echo esc_html($title); ?></h1>
	</div>
	<?php echo file_get_contents(get_template_directory_uri() . '/assets/img/wave.svg'); ?>
</section>
