<aside class="sidebar">
	 <?php 
        $category_name = "medarbejdere";
        $category = get_category_by_slug( $category_name );
        $category_id = $category->term_id;

        $args = array('child_of' => $category_id);
        $categories = get_categories( $args );
	  ?>
		
		<div class="sidebar__category">
			<h6 class="sidebar__title">Sortér</h6>

			<div class="sidebar__wrap">
				<?php foreach ($categories as $category) :  ?>
				    <div class="sidebar__filter" data-filter=".cat<?php echo $category->term_id;?>">
				    <?php echo $category->name; ?>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
</aside>