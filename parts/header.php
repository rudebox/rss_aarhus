<!doctype html>

<html <?php language_attributes(); ?> class="js">

<head>
  <script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="aa3a9ff4-e54d-4f5c-9eda-dea3774d5ddc" data-blockingmode="auto" type="text/javascript"></script>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> itemscope itemtype="https://schema.org/WebPage">

<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NXDSJJG"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>


<?php 
  //social links
  $fb = get_field('fb', 'options');
  $ig = get_field('ig', 'options');
  $yt = get_field('yt', 'options');
 ?>

<div id="transition" class="transition">

<div class="transition__container" data-namespace="general"> 

<div class="toolbar">
  <div class="toolbar__container">
    <?php lionlab_toolbar_nav() ; ?>

    <form class="toolbar__form" method="get" autocomplete="off" action="<?php bloginfo('url'); ?>/">
      <input class="toolbar__input" type="text" value="<?php the_search_query(); ?>" placeholder="<?php _e('Indtast søgeord...', 'lionlab'); ?>" name="s" id="s"></input> 
    </form>

    <div class="toolbar__social">
      <a class="toolbar__social-link" target="_blank" href="<?php echo esc_url($fb); ?>"><i class="fab fa-facebook-f"></i></a>
      <a class="toolbar__social-link" target="_blank" href="<?php echo esc_url($ig); ?>"><i class="fab fa-instagram"></i></a>
      <a class="toolbar__social-link" target="_blank" href="<?php echo esc_url($yt); ?>"><i class="fab fa-vimeo-v"></i></a>
    </div>
  
  </div>
</div>

<header class="header" itemscope itemtype="http://schema.org/WPHeader">
  <div class="wrap hpad flex flex--center flex--justify">

    <?php 
      //logo SEO markup
      $site_name = get_bloginfo( 'name' );
      $logo_markup  = ( is_front_page() ) ? '<h1 class="visuallyhidden">' . $site_name . '</h1>' : '<p class="visuallyhidden">' . $site_name . '</p>';
    ?>

    <a class="header__logo" href="<?php bloginfo('url'); ?>">
      <?php echo file_get_contents(get_template_directory_uri() . '/assets/img/logo-2.svg)'); ?>
      <?php echo $logo_markup; ?>
    </a>

    <nav class="nav" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
      <div class="nav--mobile">
        <?php lionlab_main_nav(); ?>
      </div>
    </nav>

    <div class="nav-toggle"> 
      <span class="nav-toggle__label"><?php _e('Menu', 'lionlab') ?></span> 
      <span class="nav-toggle__icon"></span>
    </div>  

  </div>

  <?php 
    $text_top = get_field('circle_text_top', 'options');
    $text_bottom = get_field('circle_text_bottom', 'options');
    $circle_link = get_field('circle_link', 'options');
  ?>

  <?php if (is_front_page() ) : ?>
  <a class="header__cta" href="<?php echo esc_url($circle_link); ?>">
    <div class="header__cta-wrap">
      <p class="header__cta-text header__cta-texttop"><?php echo $text_top; ?></p>
        <?php echo file_get_contents(get_template_directory_uri() . '/assets/img/logo_artefact.svg'); ?>
      <p class="header__cta-text header__cta-textbottom"><?php echo $text_bottom; ?></p>
    </div>
  </a>
  <?php endif; ?>
</header>
