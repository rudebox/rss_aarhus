<?php get_template_part('parts/header'); ?>

<?php 
  if ( is_tax() ) {
    $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
    $title = $term->name;
  } elseif ( is_category() ) {
    $title = single_cat_title( '', false );
  } elseif ( is_archive() ) {
    $title = post_type_archive_title( '', false );
  } elseif ( is_404() ) {
    $title = __('Siden kunne ikke findes', 'lionlab');
  } elseif ( is_search() ) {
    $title = __('Søgeresultat', 'lionlab');
  } else {
    $id = (is_home()) ? get_option('page_for_posts') : $post->ID;
    $title = get_proper_title($id);
  }

  //get thumbnail
  $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );
?>

<main>

<section class="page__hero page__hero--overlay" style="background-image: url(<?php echo esc_url($thumb[0]); ?>);">
  <div class="page__container">
    <h1 class="page__title"><?php echo esc_html($title); ?></h1>
  </div>
  <?php echo file_get_contents(get_template_directory_uri() . '/assets/img/wave.svg'); ?>
</section>

  <section class="padding--both">
    <div class="wrap hpad">
      <div class="row">

        <?php if (have_posts()): ?>
          <?php while (have_posts()): the_post(); ?>

          <article class="col-sm-10 col-sm-offset-1 blog__content" itemscope itemtype="http://schema.org/JobPosting">

            <header>
              <h2 itemprop="headline">
                  <?php the_title(); ?>
              </h2>
            </header>

            <div itemprop="description">
              <?php the_content(); ?>
            </div>

            <a class="btn btn--red" onclick="window.history.go(-1); return false;">Tilbage</a>

          </article>

        <?php endwhile; else: ?>

          <p>No posts here.</p>

        <?php endif; ?>

      </div>
    </div>
  </section>

</main>

<?php get_template_part('parts/footer'); ?>