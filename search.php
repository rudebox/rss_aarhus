<?php get_template_part('parts/header'); ?>

<main itemscope itemtype="http://schema.org/SearchResultsPage">

  <?php get_template_part('parts/page', 'header');?>


  <section class="search padding--both">
    <div class="wrap hpad">
      <?php if (have_posts()): ?>
        <h2 class="center">Din søgning for: <?php echo esc_attr(get_search_query()); ?></h2>
      <?php endif; ?>
      <div class="row flex flex--wrap">

      <?php if (have_posts()): ?>
        <?php while (have_posts()): the_post(); 
            $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );
            $img = get_field('page_img') ? : $img = get_field('page_img', 'options') ;
          ?>


          <a href="<?php the_permalink(); ?>" class="search__item col-sm-12 anim fade-up">

            <header class="search__header col-sm-6">
              <h2 class="search__title h3" itemprop="headline" title="<?php the_title_attribute(); ?>">
                  <?php the_title(); ?>
              </h2>


              <div itemprop="description">
                <?php the_excerpt(); ?>
              </div>

              <span class="btn btn--red search__btn">Læs mere</span>

            </header>

            <?php if ($thumb) : ?>
              <div class="search__thumbnail col-sm-6" style="background-image: url(<?php echo esc_url($thumb[0]); ?>)"></div>
            <?php else: ?>
               <div class="search__thumbnail col-sm-6" style="background-image: url(<?php echo esc_url($img['url']); ?>);"></div>
            <?php endif; ?>

          </a>


          <?php endwhile; else: ?>

            <div class="col-sm-12">
              <h4 class="center">Vi kunne ikke finde noget indhold der matchede din søgning for: <span><?php echo esc_attr(get_search_query()); ?></span></h4>
            </div>

        <?php endif; ?>

      </div>
    </div>

    <div class="wrap hpad">
      <?php 
        // do pagination
        do_action( 'lionlab_pagination' );
      ?>  
    </div>
  
  </section>

</main>

<?php get_template_part('parts/footer'); ?>