<?php
 /**
   * Description: Lionlab helpers
   *
   * @package Lionlab
   * @subpackage Lionlab
   * @since Version 1.0
   * @author Kaspar Rudbech
   */

	//remove countrycode from phone
	function get_formatted_phone($str) {
	  
	  // Remove +45
	  $str = str_replace('+', '00', $str);

	  // Only allow integers
	  $str = preg_replace('/[^0-9]/s', '', $str);

	  return $str;
	}


	//allow svg uploads
	function cc_mime_types($mimes) {
	  $mimes['svg'] = 'image/svg+xml';
	  return $mimes;
	}
	add_filter('upload_mimes', 'cc_mime_types');


	// Custom excerpt length
	function custom_excerpt_length( $length ) {
	  return 20;
	}

	add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


	// Custom excerpt text
	function custom_excerpt_more( $more ) {
	  return '&hellip;';
	}

	add_filter('excerpt_more', 'custom_excerpt_more');


	// Move Yoast to bottom
	function yoasttobottom() {
	  return 'low';
	}
	add_filter( 'wpseo_metabox_prio', 'yoasttobottom');


	//get proper title
	function get_proper_title( $id = null  ) {
	  
	  if ( is_null($id) ) {
	    global $post;
	    $id = $post->ID;
	  }

	  $acf_title = get_field('page_title', $id);

	  return ($acf_title) ? $acf_title : get_the_title( $id );
	}


	/**
	 * Extend WordPress search to include custom fields
	 *
	 * https://adambalee.com
	 */

	/**
	 * Join posts and postmeta tables
	 *
	 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
	 */
	function cf_search_join( $join ) {
	    global $wpdb;

	    if ( is_search() ) {    
	        $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
	    }

	    return $join;
	}
	add_filter('posts_join', 'cf_search_join' );

	/**
	 * Modify the search query with posts_where
	 *
	 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
	 */
	function cf_search_where( $where ) {
	    global $pagenow, $wpdb;

	    if ( is_search() ) {
	        $where = preg_replace(
	            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
	            "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
	    }

	    return $where;
	}
	add_filter( 'posts_where', 'cf_search_where' );

	/**
	 * Prevent duplicates
	 *
	 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
	 */
	function cf_search_distinct( $where ) {
	    global $wpdb;

	    if ( is_search() ) {
	        return "DISTINCT";
	    }

	    return $where;
	}

	add_filter( 'posts_distinct', 'cf_search_distinct' );

?>