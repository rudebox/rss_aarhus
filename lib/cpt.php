<?php 
	// Register Custom Post Type Job
	function create_job_cpt() {

		$labels = array(
			'name' => _x( 'Ledige stillinger', 'Post Type General Name', 'lionlab' ),
			'singular_name' => _x( 'Stilling', 'Post Type Singular Name', 'lionlab' ),
			'menu_name' => _x( 'Ledige stillinger', 'Admin Menu text', 'lionlab' ),
			'name_admin_bar' => _x( 'Ledige stillinger', 'Add New on Toolbar', 'lionlab' ),
			'archives' => __( 'Ledige stillinger', 'lionlab' ),
			'attributes' => __( 'Job Attributes', 'lionlab' ),
			'parent_item_colon' => __( 'Parent Job:', 'lionlab' ),
			'all_items' => __( 'Alle Ledige stillinger', 'lionlab' ),
			'add_new_item' => __( 'Tilføj Ny Ledig stilling', 'lionlab' ),
			'add_new' => __( 'Tilføj Ny', 'lionlab' ),
			'new_item' => __( 'Ny Stilling', 'lionlab' ),
			'edit_item' => __( 'Rediger Stilling', 'lionlab' ),
			'update_item' => __( 'Opdater Stilling', 'lionlab' ),
			'view_item' => __( 'Se Stilling', 'lionlab' ),
			'view_items' => __( 'Se Stillinger', 'lionlab' ),
			'search_items' => __( 'Søg Stilling', 'lionlab' ),
			'not_found' => __( 'Not found', 'lionlab' ),
			'not_found_in_trash' => __( 'Not found in Trash', 'lionlab' ),
			'featured_image' => __( 'Featured Image', 'lionlab' ),
			'set_featured_image' => __( 'Set featured image', 'lionlab' ),
			'remove_featured_image' => __( 'Remove featured image', 'lionlab' ),
			'use_featured_image' => __( 'Use as featured image', 'lionlab' ),
			'insert_into_item' => __( 'Insert into Job', 'lionlab' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Job', 'lionlab' ),
			'items_list' => __( 'Jobs list', 'lionlab' ),
			'items_list_navigation' => __( 'Jobs list navigation', 'lionlab' ),
			'filter_items_list' => __( 'Filter Jobs list', 'lionlab' ),
		);
		$rewrite = array( 
			'slug' => 'ledige-stillinger',
			'with_front' => false,
			'pages' => true,
			'feeds' => true,
		);
		$args = array(
			'label' => __( 'Ledige stillinger', 'lionlab' ),
			'description' => __( '', 'lionlab' ),
			'labels' => $labels,
			'menu_icon' => 'dashicons-businessman',
			'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
			'taxonomies' => array(),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'menu_position' => 5,
			'show_in_admin_bar' => true,
			'show_in_nav_menus' => true,
			'can_export' => true,
			'has_archive' => true,
			'hierarchical' => false,
			'exclude_from_search' => false,
			'show_in_rest' => true,
			'publicly_queryable' => true,
			'capability_type' => 'post',
			'rewrite' => $rewrite,
		);

		register_post_type( 'job', $args );

	}

	add_action( 'init', 'create_job_cpt', 0 );

	// Register Custom Post Type Medarbejder
	function create_medarbejder_cpt() {

		$labels = array(
			'name' => _x( 'Medarbejdere', 'Post Type General Name', 'lionlab' ),
			'singular_name' => _x( 'Medarbejder', 'Post Type Singular Name', 'lionlab' ),
			'menu_name' => _x( 'Medarbejdere', 'Admin Menu text', 'lionlab' ),
			'name_admin_bar' => _x( 'Medarbejder', 'Add New on Toolbar', 'lionlab' ),
			'archives' => __( 'Medarbejder Archives', 'lionlab' ),
			'attributes' => __( 'Medarbejder Attributes', 'lionlab' ),
			'parent_item_colon' => __( 'Parent Medarbejder:', 'lionlab' ),
			'all_items' => __( 'Alle Medarbejdere', 'lionlab' ),
			'add_new_item' => __( 'Tilføj Ny Medarbejder', 'lionlab' ),
			'add_new' => __( 'Tilføj Ny', 'lionlab' ),
			'new_item' => __( 'Ny Medarbejder', 'lionlab' ),
			'edit_item' => __( 'Rediger Medarbejder', 'lionlab' ),
			'update_item' => __( 'Opdater Medarbejder', 'lionlab' ),
			'view_item' => __( 'Se Medarbejder', 'lionlab' ),
			'view_items' => __( 'Se Medarbejdere', 'lionlab' ),
			'search_items' => __( 'Søg Medarbejder', 'lionlab' ),
			'not_found' => __( 'Not found', 'lionlab' ),
			'not_found_in_trash' => __( 'Not found in Trash', 'lionlab' ),
			'featured_image' => __( 'Medarbejder billede', 'lionlab' ),
			'set_featured_image' => __( 'Tilføj medarbejder billede', 'lionlab' ),
			'remove_featured_image' => __( 'Remove featured image', 'lionlab' ),
			'use_featured_image' => __( 'Use as featured image', 'lionlab' ),
			'insert_into_item' => __( 'Insert into Medarbejder', 'lionlab' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Medarbejder', 'lionlab' ),
			'items_list' => __( 'Medarbejdere list', 'lionlab' ),
			'items_list_navigation' => __( 'Medarbejdere list navigation', 'lionlab' ),
			'filter_items_list' => __( 'Filter Medarbejdere list', 'lionlab' ),
		);
		$rewrite = array( 
			'slug' => 'ansatte',
			'with_front' => false,
			'pages' => true,
			'feeds' => true,
		);
		$args = array(
			'label' => __( 'Medarbejder', 'lionlab' ),
			'description' => __( '', 'lionlab' ),
			'labels' => $labels,
			'menu_icon' => 'dashicons-groups',
			'supports' => array('title', 'thumbnail'),
			'taxonomies' => array('category'),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'menu_position' => 5,
			'show_in_admin_bar' => true,
			'show_in_nav_menus' => true,
			'can_export' => true,
			'has_archive' => true,
			'hierarchical' => false,
			'exclude_from_search' => false,
			'show_in_rest' => true,
			'publicly_queryable' => true,   
			'capability_type' => 'post',
			'rewrite' => $rewrite,
		);
		register_post_type( 'medarbejder', $args );

	}

	add_action( 'init', 'create_medarbejder_cpt', 0 );
?>