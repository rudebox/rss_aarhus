<?php get_template_part('parts/header'); ?>

<main>

  <?php get_template_part('parts/page', 'header');?>

  <section class="padding--both">
  	<div class="wrap hpad center">

    	<h2>Beklager, Men vi kunne ikke finde den side du søgte efter.</h2>
    	<a class="btn btn--red" href="/">Gå til forsiden</a>

    </div>
  </section>

</main>

<?php get_template_part('parts/footer'); ?>