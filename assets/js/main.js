jQuery(document).ready(function($) {


    //in viewport check
    var $animation_elements = $('.anim');
    var $window = $(window);

    function check_if_in_view() {
      var window_height = $window.height();
      var window_top_position = $window.scrollTop();
      var window_bottom_position = (window_top_position + window_height);
     
      $.each($animation_elements, function() {
        var $element = $(this);
        var element_height = $element.outerHeight();
        var element_top_position = $element.offset().top;
        var element_bottom_position = (element_top_position + element_height);
     
        //check to see if this current container is within viewport
        if ((element_top_position <= window_bottom_position)) {
          $element.addClass('in-view');
        } else {
          $element.removeClass('in-view');
        }
      });
    }

    $window.on('scroll resize', check_if_in_view);
    $window.trigger('scroll');

    //menu toggle
    $('.nav-toggle').click(function(e) {
      e.preventDefault();
      $('.nav--mobile').toggleClass('is-open');
      $('body').toggleClass('is-fixed');

      $('.nav__item').not('.nav__dropdown > .nav__item').each(function(index) {        
          var delayNumber = index * 100;
          
          $(this).delay(delayNumber).queue(function(next) {
            $(this).toggleClass('is-animated');
            next();
          });  

      });
    });


    //mobile on click dropdown
    if ($(window).width() < 768) {

      $('ul.nav__menu').find('> li').click(
         function() {
             $(this).find('> ul').toggleClass('is-visible');
         }
      );


      $('ul.nav__dropdown').find('> li').click(
         function(e) {
             e.stopPropagation()
             $(this).find('> ul').toggleClass('is-visible');
         }
      );
    }


    //mixit up
    if($('body').is('.post-type-archive-medarbejder')){

      // Instantiate and configure the mixer
      var mixer = mixitup('.mixit', {
        load: {
          filter: '.cat4'
        }
      });

    }
    


    //owl slider/carousel
    var owl = $(".slider__track");

    owl.owlCarousel({
        loop: false,
        items: 1,
        autoplay: true,
        // nav: true,
        dots: true,
        autplaySpeed: 11000,
        autoplayTimeout: 10000,
        smartSpeed: 250,
        smartSpeed: 2200,
        navSpeed: 2200
        // navText : ["<i class='fa fa-arrow-left' aria-hidden='true'></i>", "<i class='fa fa-arrow-right' aria-hidden='true'></i>"]
    });

  }); 
