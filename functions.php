<?php

/* Do not remove this line. */
require_once('includes/scratch.php');
require_once('lib/theme-dependencies.php');


/*
 * adds all meta information to the <head> element for us.
 */

function scratch_meta() { ?>

  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="description" content="<?php bloginfo('description'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="apple-touch-icon" href="/apple-touch-icon.png">
  <!-- Place favicon.ico in the root directory -->

  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NXDSJJG');</script>

<?php }

add_action('wp_head', 'scratch_meta');

/**
 * Add fonts to header for preload option insted of importing through css in variables
 */
function lionlab_load_fonts() { ?>
  <link rel="preload" as="font" crossorigin="anonymous" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/OpenSans-bold.ttf" type="font/ttf"> 
  <link rel="preload" as="font" crossorigin="anonymous" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/OpenSans-Light.ttf" type="font/ttf">
  <link rel="preload" as="font" crossorigin="anonymous" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/baar-atropos.ttf" type="font/ttf">  
<?php }



/* Theme CSS */

function theme_styles() {

  wp_enqueue_style( 'normalize', 'https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css', false, null );

  wp_enqueue_style( 'fontawesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css', false, null );

  wp_enqueue_style( 'fancybox', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.6/jquery.fancybox.min.css', true, null );

  wp_register_style( 'scratch-main', get_template_directory_uri() . '/assets/css/master.css', false, filemtime(dirname(__FILE__) . '/assets/css/master.css') );
  wp_enqueue_style( 'scratch-main' );

}

add_action( 'wp_enqueue_scripts', 'theme_styles' );

/* Theme JavaScript */

function theme_js() {

  if (!is_admin()) {
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js', array(), null, true );
  }

  wp_register_script( 'scratch-main-concat', get_template_directory_uri() . '/assets/js/concat/main.js', array('jquery'), filemtime(dirname(__FILE__) . '/assets/js/concat/main.js'), true ); 

  wp_register_script( 'scratch-main-min', get_template_directory_uri() . '/assets/js/build/main.min.js', array('jquery'), filemtime(dirname(__FILE__) . '/assets/js/build/main.min.js'), true );

  /* FOR DEVELOPMENT */
  // wp_enqueue_script( 'scratch-main-concat' );

  /* FOR PRODUCTION */
  wp_enqueue_script( 'scratch-main-min' );

  wp_enqueue_script( 'fancybox', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.6/jquery.fancybox.min.js', array(), null, true ); 

}

add_action( 'wp_enqueue_scripts', 'theme_js' );


/* Enable ACF Options Pages */


if( function_exists('acf_add_options_page') ) {
  
  acf_add_options_page(array(
    'page_title'  => 'Globalt indhold',
    'menu_title'  => 'Globalt indhold',
    'menu_slug'   => 'global-content',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));
  
  acf_add_options_sub_page(array(
    'page_title'  => 'Header',
    'menu_title'  => 'Header',
    'parent_slug' => 'global-content',
  ));

  acf_add_options_sub_page(array(
    'page_title'  => 'Sidebar',
    'menu_title'  => 'Sidebar',
    'parent_slug' => 'global-content',
  ));
  
  acf_add_options_sub_page(array(
    'page_title'  => 'Footer',
    'menu_title'  => 'Footer',
    'parent_slug' => 'global-content',
  ));

  acf_add_options_sub_page(array(
      'page_title'      => 'Medarbejder Arkiv indstillinger',
      'parent_slug'     => 'edit.php?post_type=medarbejder', 
      'capability' => 'manage_options'
  ));

}

/* Enable Featured Image */

add_theme_support( 'post-thumbnails' );

// Image sizes
add_image_size('gallery', 600, 444, true);

// Add created images sizes to dropdown in WP control panel
add_filter( 'image_size_names_choose', 'custom_image_sizes' );

function custom_image_sizes( $sizes ) {
  return array_merge( $sizes, array( 
    
  ) );
} 

/* Enable Custom Menus */

add_theme_support( 'menus' );

register_nav_menus(
  array(
    'lionlab-main-nav' => __( 'Main Nav', 'lionlab' ),   // main nav in header
    'lionlab-toolbar-nav' => __( 'Toolbar Nav', 'lionlab' )   // main nav in header
  )
);

function lionlab_main_nav() {
  // display the wp3 menu if available
  wp_nav_menu(array(
    'container' => false, // remove nav container
    'container_class' => '', // class of container (should you choose to use it)
    'menu' => __( 'Main Nav', 'lionlab' ), // nav name
    'menu_class' => 'nav__menu', // adding custom nav class
    'theme_location' => 'lionlab-main-nav', // where it's located in the theme
    'before' => '', // before the menu
    'after' => '', // after the menu
    'link_before' => '', // before each link
    'link_after' => '', // after each link
    'depth' => 0,    // fallback function
    'walker' => new Clean_Walker_Nav () //remove wordpress walker
  ));
} /* end lionlab main nav */

function lionlab_toolbar_nav() {
  // display the wp3 menu if available
  wp_nav_menu(array(
    'container' => false, // remove nav container
    'container_class' => '', // class of container (should you choose to use it)
    'menu' => __( 'toolbar Nav', 'lionlab' ), // nav name
    'menu_class' => 'nav__menu nav__menu--toolbar', // adding custom nav class
    'theme_location' => 'lionlab-toolbar-nav', // where it's located in the theme
    'before' => '', // before the menu
    'after' => '', // after the menu
    'link_before' => '', // before each link
    'link_after' => '', // after each link
    'depth' => 0,    // fallback function
    'walker' => new Clean_Walker_Nav () //remove wordpress walker
  ));
} /* end lionlab toolbar nav */


//change post label name
function change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Aktuelt';
    $submenu['edit.php'][5][0] = 'Aktuelt';
    $submenu['edit.php'][10][0] = 'Tilføj indlæg';
    $submenu['edit.php'][16][0] = 'Aktuelt Tags';
    echo '';
}

function change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Aktuelt';
    $labels->singular_name = 'Aktuelt';
    $labels->add_new = 'Tilføj indlæg';
    $labels->add_new_item = 'Tilføj indlæg';
    $labels->edit_item = 'Rediger indlæg';
    $labels->new_item = 'Aktuelt';
    $labels->view_item = 'Se indlæg';
    $labels->search_items = 'Søg indlæg';
    $labels->not_found = 'Ingen indlæg fundet';
    $labels->not_found_in_trash = 'Ingen indlæg fundet i papirkurven';
    $labels->all_items = 'Alle indlæg';
    $labels->menu_name = 'Aktuelt';
    $labels->name_admin_bar = 'Aktuelt';
}
 
add_action( 'admin_menu', 'change_post_label' );
add_action( 'init', 'change_post_object' );

//redirect single medarbejder custom post type
add_action( 'template_redirect', 'wpse_128636_redirect_post' );

function wpse_128636_redirect_post() {
  if ( is_singular( 'medarbejder' ) ) {
    wp_redirect( home_url(), 301 );
    exit;
  }
}


/* Place custom functions below here. */

/* Don't delete this closing tag. */
?>
