<?php get_template_part('parts/header'); ?>

<main>

<?php get_template_part('parts/page', 'header'); ?>

  <section class="padding--both">
    <div class="wrap hpad">
      <div class="row">

        <?php if (have_posts()): ?>
          <?php while (have_posts()): the_post(); ?>

          <article class="col-sm-10 col-sm-offset-1 blog__content" itemscope itemtype="http://schema.org/JobPosting">

            <header>
              <h2 itemprop="headline">
                  <?php the_title(); ?>
              </h2>
            </header>

            <div itemprop="description">
              <?php the_content(); ?>
            </div>

            <a class="btn btn--red" onclick="window.history.go(-1); return false;">Tilbage</a>

          </article>

        <?php endwhile; else: ?>

          <p>No posts here.</p>

        <?php endif; ?>

      </div>
    </div>
  </section>

</main>

<?php get_template_part('parts/footer'); ?>