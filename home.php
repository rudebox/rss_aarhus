<?php get_template_part('parts/header'); ?>

<main>

<?php get_template_part('parts/page', 'header'); ?>

  <section class="blog padding--both">
    <div class="wrap hpad">
      <div class="row flex flex--wrap">

        <?php if (have_posts()): ?>
          <?php while (have_posts()): the_post(); 
                //get thumbnail
                $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );
            ?>

            <a href="<?php the_permalink(); ?>" class="blog__item col-sm-12 anim fade-up" itemscope itemtype="http://schema.org/BlogPosting">

              <header class="blog__header col-sm-6">
                <h2 class="blog__title" itemprop="headline">                
                    <?php the_title(); ?>
                </h2>

                <div itemprop="description">
                  <?php the_excerpt(); ?>
                </div>

                <span class="btn btn--red blog__btn">Læs mere</span>
              </header>

              <?php if ($thumb) : ?>
                <div class="blog__thumbnail col-sm-6" style="background-image: url(<?php echo esc_url($thumb[0]); ?>)"></div>
              <?php endif; ?>

            </a>

          <?php endwhile; else: ?>

            <h2><?php _e('Der er øjeblikket ingen indlæg i denne kategori', 'lionlab') ?></h2>

        <?php endif; ?>

      </div>
    </div>
  </section>

</main>

<?php get_template_part('parts/footer'); ?>