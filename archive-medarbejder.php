<?php get_template_part('parts/header'); ?>

<main>

<?php get_template_part('parts/page', 'header-cpt'); ?>

  <section class="employees padding--both">

    <div class="wrap hpad">

      <div class="row flex flex--wrap">
        
        <div class="col-md-2 sidebar__col">
          <?php 
            get_template_part('parts/sidebar'); 
          ?>
        </div> 

          <div class="col-md-10 employees__mixit">
            <?php 
              $category_name = "medarbejdere";
              $category = get_category_by_slug( $category_name );
              $category_id = $category->term_id;

              $args = array('child_of' => $category_id);
              $categories = get_categories( $args );
            ?>

            <div class="row flex flex--wrap mixit">

            <?php 

              //query arguments
              $args = array(
                'posts_per_page' => -1,
                'post_type' => 'medarbejder',
                'orderby' => 'menu_order',
                'order' => 'ASC'
              );
               
              $query = new WP_QUERY($args);
             ?>

            <?php if ($query->have_posts()): ?>
              <?php while ($query->have_posts()): $query->the_post(); ?>

              <?php   
                //get thumb
                $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );
                //post img alt tag
                $alt = get_post_meta($thumb, '_wp_attachment_image_alt', true);

                $position = get_field('contact_position');
                $mail = get_field('contact_mail');
                $phone = get_field('contact_phone'); 

                $cats = get_the_category();
                $cat_string = "";

                foreach ($cats as $cat) {
                  $cat_string .= " cat" . $cat->term_id ."";
                }
              ?>

              <div class="col-sm-4 employees__item mix <?php echo esc_attr($cat_string); ?>">
                <?php if ($thumb) : ?>
                <div class="employees__img" style="background-image: url(<?php echo esc_url($thumb[0]); ?>);"></div>
                <?php endif; ?>
                <div class="employees__content">
                  <h5 class="employees__name"><?php the_title(); ?></h5>
                  <h6 class="employees__position red"><?php echo esc_html($position); ?></h6>
                  
                  <?php if ($mail) : ?>
                   E-mail: <a class="employees__mail" href="mailto:<?php echo esc_html($mail); ?>"><?php echo esc_html($mail); ?></a><br>
                  <?php endif; ?>

                  <?php if ($phone) : ?>
                  Tlf: <a class="employees__phone" href="tel:<?php echo get_formatted_phone($phone); ?>"><?php echo esc_html($phone); ?></a>
                  <?php endif; ?>
                  
                </div>
              </div>

              <?php endwhile; wp_reset_postdata(); else: ?>

                <p>Ingen produkter i denne kategori fundet.</p>

            <?php endif; ?>
            </div>
        </div>

      </div>
    </div>
  </section>

</main>

<?php get_template_part('parts/footer'); ?>